package pl.prabel.githubdemo.presenter.main;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.jakewharton.rxbinding.view.RxView;

import javax.annotation.Nonnull;

import butterknife.ButterKnife;
import pl.prabel.githubdemo.BR;
import pl.prabel.githubdemo.api.model.RepoModel;
import rx.Subscription;
import rx.functions.Func1;

public class RepositoriesViewHolder extends RecyclerView.ViewHolder {

    private final ViewDataBinding bind;

    private Subscription subscription;

    @Nonnull
    private RepositoriesAdapter.Listener listener;

    public RepositoriesViewHolder(View itemView, RepositoriesAdapter.Listener listener) {
        super(itemView);
        bind = DataBindingUtil.bind(itemView);
        ButterKnife.bind(this, itemView);
        this.listener = listener;
    }

    public void bind(final RepoModel item) {
        bind.setVariable(BR.item, item);

        subscription = RxView.clicks(itemView)
                .map(new Func1<Void, RepoModel>() {
                    @Override
                    public RepoModel call(Void aVoid) {
                        return item;
                    }
                })
                .subscribe(listener.clickRepoAction());
    }

    public void recycle() {
        if (subscription != null) {
            subscription.unsubscribe();
            subscription = null;
        }
    }
}
