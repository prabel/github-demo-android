package pl.prabel.githubdemo.presenter.issue;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.appunite.detector.ChangesDetector;
import com.appunite.detector.SimpleDetector;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import pl.prabel.githubdemo.R;
import pl.prabel.githubdemo.api.model.IssueModel;
import rx.Observer;
import rx.functions.Action1;

public class IssuesAdapter extends RecyclerView.Adapter<IssueViewHolder>
        implements Action1<ImmutableList<IssueModel>>, ChangesDetector.ChangesAdapter {

    public interface Listener {
        @Nonnull
        Observer<IssueModel> clickIssueAction();
    }

    @Nonnull
    private ImmutableList<IssueModel> items = ImmutableList.of();
    @Nonnull
    private final ChangesDetector<IssueModel, IssueModel> changesDetector;
    @Nonnull
    private Listener listener;
    @Nonnull
    private final LayoutInflater inflater;

    @Inject
    public IssuesAdapter(@Nonnull final LayoutInflater inflater,
                               @Nonnull final Listener listener) {
        this.inflater = inflater;
        this.listener = listener;
        this.changesDetector = new ChangesDetector<>(new SimpleDetector<IssueModel>());
    }

    @Override
    public void call(ImmutableList<IssueModel> issueModelImmutableList) {
        this.items = issueModelImmutableList;
        changesDetector.newData(this, items, false);
    }

    @Override
    public IssueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new IssueViewHolder(inflater.inflate(R.layout.issue_item, parent, false), listener);
    }

    @Override
    public void onBindViewHolder(IssueViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
