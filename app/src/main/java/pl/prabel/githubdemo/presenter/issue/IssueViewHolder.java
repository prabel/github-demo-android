package pl.prabel.githubdemo.presenter.issue;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.jakewharton.rxbinding.view.RxView;

import javax.annotation.Nonnull;

import butterknife.ButterKnife;
import pl.prabel.githubdemo.BR;
import pl.prabel.githubdemo.api.model.IssueModel;
import rx.Subscription;
import rx.functions.Func1;

public class IssueViewHolder extends RecyclerView.ViewHolder {

    private final ViewDataBinding bind;

    private Subscription subscription;

    @Nonnull
    private IssuesAdapter.Listener listener;

    public IssueViewHolder(View itemView, IssuesAdapter.Listener listener) {
        super(itemView);
        bind = DataBindingUtil.bind(itemView);
        ButterKnife.bind(this, itemView);
        this.listener = listener;
    }

    public void bind(final IssueModel item) {
        bind.setVariable(BR.item, item);

        subscription = RxView.clicks(itemView)
                .map(new Func1<Void, IssueModel>() {
                    @Override
                    public IssueModel call(Void aVoid) {
                        return item;
                    }
                })
                .subscribe(listener.clickIssueAction());
    }

    public void recycle() {
        if (subscription != null) {
            subscription.unsubscribe();
            subscription = null;
        }
    }
}
