package pl.prabel.githubdemo.presenter.issue;

import android.support.annotation.NonNull;
import android.util.Log;

import com.appunite.rx.ObservableExtensions;
import com.appunite.rx.ResponseOrError;
import com.appunite.rx.dagger.NetworkScheduler;
import com.appunite.rx.dagger.UiScheduler;
import com.appunite.rx.operators.MoreOperators;
import com.google.common.collect.ImmutableList;

import javax.inject.Inject;

import pl.prabel.githubdemo.api.ApiService;
import pl.prabel.githubdemo.api.model.IssueModel;
import pl.prabel.githubdemo.api.model.RepoModel;
import rx.Observable;
import rx.Observer;
import rx.Scheduler;
import rx.subjects.PublishSubject;

public class IssuesPresenter {
    @NonNull
    private final Observable<ResponseOrError<ImmutableList<IssueModel>>> issuesErrorObservable;
    @NonNull
    private final PublishSubject<Object> refreshSubject = PublishSubject.create();
    @NonNull
    private final PublishSubject<IssueModel> issueClickSubject = PublishSubject.create();

    @Inject
    public IssuesPresenter(@NonNull final ApiService apiService,
                           @NonNull @NetworkScheduler final Scheduler networkScheduler,
                           @NonNull @UiScheduler final Scheduler uiScheduler,
                           @NonNull final RepoModel repoModel) {

        issuesErrorObservable = apiService.getIssues(repoModel.getOwner().getLogin(), repoModel.getName())
                .subscribeOn(networkScheduler)
                .observeOn(uiScheduler)
                .compose(ResponseOrError.<ImmutableList<IssueModel>>toResponseOrErrorObservable())
                .compose(MoreOperators.<ImmutableList<IssueModel>>repeatOnError(networkScheduler))
                .compose(MoreOperators.<ResponseOrError<ImmutableList<IssueModel>>>cacheWithTimeout(networkScheduler))
                .compose(MoreOperators.<ResponseOrError<ImmutableList<IssueModel>>>refresh(refreshSubject))
                .compose(ObservableExtensions.<ResponseOrError<ImmutableList<IssueModel>>>behaviorRefCount());
    }

    @NonNull
    public Observable<Boolean> getProgressObservable() {
        return ResponseOrError.combineProgressObservable(
                ImmutableList.of(ResponseOrError.transform(issuesErrorObservable)));
    }

    @NonNull
    public Observable<Throwable> getErrorObservable() {
        return issuesErrorObservable
                .compose(ResponseOrError.<ImmutableList<IssueModel>>onlyError());
    }

    @NonNull
    public Observable<ImmutableList<IssueModel>> getRepositoriesObservable() {
        return issuesErrorObservable
                .compose(ResponseOrError.<ImmutableList<IssueModel>>onlySuccess());
    }

    @NonNull
    public Observer<IssueModel> issueClickObserver() {
        return issueClickSubject;
    }

    @NonNull
    public Observable<IssueModel> issueClickObservable() {
        return issueClickSubject;
    }

}
