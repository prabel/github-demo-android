package pl.prabel.githubdemo.presenter.issue;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.appunite.rx.functions.BothParams;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.jakewharton.rxbinding.view.RxView;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import dagger.Provides;
import pl.prabel.githubdemo.R;
import pl.prabel.githubdemo.api.model.IssueModel;
import pl.prabel.githubdemo.api.model.RepoModel;
import pl.prabel.githubdemo.api.throwable.NoConnectionException;
import pl.prabel.githubdemo.content.AppConst;
import pl.prabel.githubdemo.dagger.ActivityModule;
import pl.prabel.githubdemo.dagger.ActivityScope;
import pl.prabel.githubdemo.dagger.ApplicationComponent;
import pl.prabel.githubdemo.dagger.BaseActivityComponent;
import pl.prabel.githubdemo.presenter.BaseActivity;
import pl.prabel.githubdemo.rx.CustomViewAction;
import rx.Observer;
import rx.functions.Func1;

public class IssueActivity extends BaseActivity implements IssuesAdapter.Listener{

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.progress_view)
    View progressView;
    @Bind(R.id.container)
    View container;

    @Inject
    IssuesPresenter presenter;
    @Inject
    IssuesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        initViews();
        initPresenters();
    }

    private void initViews() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter.setHasStableIds(true);
        recyclerView.setAdapter(adapter);
    }

    private void initPresenters() {
        presenter.getProgressObservable()
                .compose(lifecycleMainObservable().<Boolean>bindLifecycle())
                .subscribe(RxView.visibility(progressView));

        presenter.getRepositoriesObservable()
                .compose(lifecycleMainObservable().<ImmutableList<IssueModel>>bindLifecycle())
                .subscribe(adapter);

        presenter.getErrorObservable()
                .compose(lifecycleMainObservable().<Throwable>bindLifecycle())
                .map(new Func1<Throwable, String>() {
                    @Override
                    public String call(Throwable throwable) {
                        if (throwable != null && throwable.getCause() instanceof NoConnectionException) {
                            return getString(R.string.no_connection_error);
                        }
                        return getString(R.string.unknown_error);
                    }
                })
                .subscribe(new CustomViewAction.SnackBarErrorMessageAction(container));
    }

    @Nonnull
    @Override
    public BaseActivityComponent createActivityComponent(@Nullable Bundle savedInstanceState, ApplicationComponent appComponent) {
        final Component component = DaggerIssueActivity_Component.builder().applicationComponent(appComponent)
                .activityModule(new ActivityModule(this))
                .module(new Module(this))
                .build();

        component.inject(this);
        return component;
    }

    @Nonnull
    @Override
    public Observer<IssueModel> clickIssueAction() {
        return presenter.issueClickObserver();
    }

    @dagger.Module
    public class Module {
        private IssueActivity issueActivity;
        private RepoModel repoModel;

        public Module(IssueActivity issueActivity) {
            this.issueActivity = issueActivity;

            if (issueActivity.getIntent() != null) {
                repoModel = issueActivity.getIntent().getParcelableExtra(AppConst.REPO_MODEL);
            }
        }

        @Provides
        RepoModel provideRepoModel() {
            return repoModel;
        }

        @Provides
        IssuesAdapter.Listener provideListener() {
            return issueActivity;
        }
    }

    @ActivityScope
    @dagger.Component(
            dependencies = ApplicationComponent.class,
            modules = {
                    ActivityModule.class,
                    IssueActivity.Module.class
            }
    )
    interface Component extends BaseActivityComponent{
        void inject(IssueActivity issueActivity);

        IssuesAdapter.Listener listener();
    }
}
