package pl.prabel.githubdemo.presenter.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.appunite.detector.ChangesDetector;
import com.appunite.detector.SimpleDetector;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import pl.prabel.githubdemo.R;
import pl.prabel.githubdemo.api.model.RepoModel;
import rx.Observer;
import rx.functions.Action1;

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesViewHolder>
        implements Action1<ImmutableList<RepoModel>>, ChangesDetector.ChangesAdapter {

    public interface Listener {
        @Nonnull
        Observer<RepoModel> clickRepoAction();
    }

    @Override
    public RepositoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RepositoriesViewHolder(inflater.inflate(R.layout.repo_item, parent, false), listener);
    }

    @Override
    public void onBindViewHolder(RepositoriesViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onViewRecycled(RepositoriesViewHolder holder) {
        holder.recycle();
        super.onViewRecycled(holder);
    }

    @Nonnull
    private ImmutableList<RepoModel> items = ImmutableList.of();
    @Nonnull
    private final ChangesDetector<RepoModel, RepoModel> changesDetector;
    @Nonnull
    private Listener listener;
    @Nonnull
    private final LayoutInflater inflater;

    @Inject
    public RepositoriesAdapter(@Nonnull final LayoutInflater inflater,
                               @Nonnull final Listener listener) {
        this.inflater = inflater;
        this.listener = listener;
        this.changesDetector = new ChangesDetector<>(new SimpleDetector<RepoModel>());
    }

    @Override
    public void call(ImmutableList<RepoModel> repoModelImmutableList) {
        this.items = repoModelImmutableList;
        changesDetector.newData(this, items, false);
    }
}
