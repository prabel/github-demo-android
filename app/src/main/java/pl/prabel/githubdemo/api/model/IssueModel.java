package pl.prabel.githubdemo.api.model;

import com.appunite.detector.SimpleDetector;

import javax.annotation.Nonnull;

public class IssueModel implements SimpleDetector.Detectable<IssueModel> {

    private final String title;

    public IssueModel(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean matches(@Nonnull IssueModel issueModel) {
        return false;
    }

    @Override
    public boolean same(@Nonnull IssueModel issueModel) {
        return false;
    }
}
