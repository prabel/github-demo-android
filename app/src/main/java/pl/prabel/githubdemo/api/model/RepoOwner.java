package pl.prabel.githubdemo.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.Objects;

public class RepoOwner implements Parcelable{

    private final String login;
    private final String id;

    public RepoOwner(String login, String id) {
        this.login = login;
        this.id = id;
    }

    protected RepoOwner(Parcel in) {
        login = in.readString();
        id = in.readString();
    }

    public static final Creator<RepoOwner> CREATOR = new Creator<RepoOwner>() {
        @Override
        public RepoOwner createFromParcel(Parcel in) {
            return new RepoOwner(in);
        }

        @Override
        public RepoOwner[] newArray(int size) {
            return new RepoOwner[size];
        }
    };

    public String getLogin() {
        return login;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RepoOwner)) return false;
        RepoOwner repoOwner = (RepoOwner) o;
        return Objects.equal(login, repoOwner.login) &&
                Objects.equal(id, repoOwner.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(login, id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(login);
        dest.writeString(id);
    }
}
